## Build
FROM gradle:7.6-jdk17-alpine as builder
WORKDIR /app
COPY build.gradle settings.gradle ./
COPY src src
RUN gradle assemble --no-daemon

## Deploy
FROM eclipse-temurin:17-jre-alpine
VOLUME /tmp
COPY --from=builder /app/build/libs/*.jar app.jar
ENTRYPOINT ["sh", "-c", "java -jar /app.jar"]
