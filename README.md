# Spring Boot with Maven
Spring Boot - Hello World 애플리케이션 Gradle CI/CD 파이프라인 예제
#### 환경
- java 17
- Gradle 
- Spring Boot 3.0.2
- Dependency
  - Spring Boot web 
  - devtools starters
  - Project Lombok
  - Spring Boot test starter


#### 파이프라인 정보
- **Spring Boot Gradle with Helm**
  <!-- [gradle:build, gradle:test, security:semgrep-sast, security:code-quality, security:secret_detection, security:container_scanning, docker:kaniko, deploy:helm] -->
